#!/usr/bin/env python
from setuptools import setup


setup(
    name='deploy_setup',
    version='0.1',
    author='Radek Czajka',
    author_email='rczajka@rczajka.pl',
    url='',
    install_requires=['git-remote-run>=1.2'],
    packages=['deploy_setup'],
    package_data={
        'deploy_setup': ['templates/*'],
    },
    entry_points={
        'console_scripts': [
            'deploy-setup = deploy_setup.cli:run',
        ],
    },
    license='LICENSE',
    description='.',
    long_description="",
)
