import argparse
from . import deploy_setup


def run():
    parser = argparse.ArgumentParser(prog='deploy-setup')
    parser.add_argument('remote', help='Name of the git remote to setup the deployment around.')

    args = parser.parse_args()

    deploy_setup(args.remote)

