# Makefile helper, supplying configuration files,
# defined as make prerequisites in the project's Makefile.
#
# For example, project's Makefile might say:
#
# deploy: src/core/local_settings.py
# 	pip install ...
# 	...
#
# This file fulfills this prerequisite by looking for local_settings.py
# in the directory it sits in, and copying it to the needed location.
#
# This file expects to be used as:
#
# 	make -C (release-dir) -f (path-to-this-file) ...
#
# and assumes there's a Makefile in the release directory.


HERE := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))


# This loads the Makefile from the release directory.
include Makefile


# Match any needed path, if we can find a matching file for it, and copy it.
.SECONDEXPANSION:
%: $(HERE)$$(notdir $$@)
	cp $< $@


# Avoid circular rules.
$(HERE)%: 

