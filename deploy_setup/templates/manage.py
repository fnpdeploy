#!/bin/bash
# Simple wrapper for running current manage.py with the right Python.
HERE="$( dirname "${BASH_SOURCE[0]}" )"
source "$HERE/etc/deployment.cfg"
VE="$(cd "$HERE"; realpath "$VIRTUALENV_PATH")"
exec $VE/bin/python $HERE/releases/current/src/manage.py "$@"
